<?php

/**
 * You can use this so filter the options shown to the end user.
 */
function HOOK_block_instance_field_available_options(&$options, $items, $element) {

}

/**
 * You can use this to alter the subforms of blocks inside the field widget.
 */
function HOOK_form_block_instance_config_form(&$form, $plugin_block, $configuration) {

}